#!/usr/bin/python3
#    Copyright (C) 2018 Man Kai KAM
#    This file is part of Tracemail.
#
#    Tracemail is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Tracemail is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Tracemail.  If not, see <http://www.gnu.org/licenses/>.
import paramiko
from scp import SCPClient
import sys
import boto3
import time
IMAGEID = 'ami-597d553c'
SECGROUPID = ["your amazon security group id"]
KEY = "mykey"
KEYFILE = "./mykey.pem"
ITYPE = 't2.micro'
DEBUG = False

def createvm():
  ec2 = boto3.resource('ec2')
  instance = ec2.create_instances(
      ImageId=IMAGEID,
      MinCount=1,
      MaxCount=1,
      SecurityGroupIds=SECGROUPID,
      KeyName=KEY,
      InstanceType=ITYPE)
  while "running" not in instance[0].state['Name'] :
    print(instance[0].state['Name'])
    time.sleep(3)
    instance[0].load()
  return instance[0].id, instance[0].public_ip_address

try:
  uploadfile=sys.argv[1]
  iid, pip = createvm()
  print(iid)
  print(pip)
  time.sleep(5)
  k = paramiko.RSAKey.from_private_key_file(KEYFILE)
  ssh = paramiko.SSHClient()
  ssh.load_system_host_keys()
  ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  ssh.connect(pip, username = "ubuntu", pkey = k, port=22)
  with SCPClient(ssh.get_transport()) as scp:
    scp.put(uploadfile)
  commands = ["sudo apt-get -y install apache2",
              "sudo a2enmod ssl",
              "sudo a2ensite default-ssl",
              "sudo systemctl restart apache2",
              "chmod 755 " + uploadfile + " ; sudo mv " + uploadfile + " /var/www/html/"]
  for command in commands:
    stdin, stdout, stderr = ssh.exec_command(command)
    if DEBUG:
      print("EXEC: " + command)
      print(stdout.read())
      print( "Errors")
      print(stderr.read())
  ssh.close()
  print(iid)
  print("https://" + pip + "/" + uploadfile)
except:
  exc_type, exc_obj, exc_tb = sys.exc_info()
  print("error:", exc_type)
  print("line:",  exc_tb.tb_lineno)
  
sys.exit(0)
