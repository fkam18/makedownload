# makedownload.py

makedownload.py is a lean tool to on-the-fly create an AWS ec2 instance, upload a file, return an https link, so you can share your file to another person just with the link. Later will add a utility to auto delete the instance.


## Getting Started

- edit makedownload.py and set the constants:
SECGROUPID = ["your amazon security group id"] --- in that group allows https and ssh
KEY = "mykey"  --- set it up in your aws console
KEYFILE = "./mykey.pem"

- e.g run it as:
./makedownload.py file1 (assume file1 is the file to upload to public sharing)

-- make sure your key file location is set above

### Prerequisites

python3
all the import's in the program

### Installing

--- just a script

## Contributing

Please contribute.

## Versioning

Please suggest.

## Authors

* **Man Kai KAM** - *Initial work* - https://gitlab.com/fkam18/makedownload


## License

This project is licensed under the GPL V3.

## Acknowledgments

* Hat tip to anyone who's code was used
* Love python3

